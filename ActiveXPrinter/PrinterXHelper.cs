﻿namespace ActiveXPrinter
{
    using System;
    using System.Reflection;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
    using Microsoft.Win32;

    internal class PrinterXHelper : AxHost
    {
        protected const int OlemiscRecomposeonresize = 1;
        protected const int OlemiscCantlinkinside = 16;
        protected const int OlemiscInsideout = 128;
        protected const int OlemiscActivatewhenvisible = 256;
        protected const int OlemiscSetclientsitefirst = 131072;

        internal PrinterXHelper() 
            : base(null)
        {
        }

        public static void RegasmRegisterControl(Type t)
        {
            // Check the argument
            GuardNullType(t, "t");
            GuardTypeIsControl(t);

            // Open the CLSID key of the control
            using (var keyClsid = Registry.ClassesRoot.OpenSubKey(
                @"CLSID\" + t.GUID.ToString("B"), true))
            {
                // Set "InprocServer32" to register a 32-bit in-process server.
                // InprocServer32 = <path to 32-bit inproc server>
                // Ref: http://msdn.microsoft.com/en-us/library/ms683844.aspx
                var subkey = keyClsid.OpenSubKey("InprocServer32", true);
                if (subkey != null)

                // .NET runtime engine (mscoree.dll) for .NET assemblies
                {
                    subkey.SetValue(null, Environment.SystemDirectory + @"\mscoree.dll");
                }


                // Create "Control" to identify it as an ActiveX Control.
                // Ref: http://msdn.microsoft.com/en-us/library/ms680056.aspx
                using (subkey = keyClsid.CreateSubKey("Control"))
                {
                }


                // Create "MiscStatus" to specify how to create/display an object. 
                // MiscStatus = <combination of values from OLEMISC enumeration>
                // Ref: http://msdn.microsoft.com/en-us/library/ms683733.aspx
                using (subkey = keyClsid.CreateSubKey("MiscStatus"))
                {
                    const int MiscStatus = OlemiscRecomposeonresize +
                                           OlemiscCantlinkinside + OlemiscInsideout +
                                           OlemiscActivatewhenvisible + OlemiscSetclientsitefirst;

                    subkey.SetValue(string.Empty, MiscStatus.ToString(), RegistryValueKind.String);
                }


                // Create "ToolBoxBitmap32" to identify the module name and the resource  
                // ID for a 16 x 16 bitmap as the toolbar button face.
                // ToolBoxBitmap32 = <filename>.<ext>, <resourceID>
                // Ref: http://msdn.microsoft.com/en-us/library/ms687316.aspx
                using (subkey = keyClsid.CreateSubKey("ToolBoxBitmap32"))
                {
                    // If you want different icons for each control in the assembly you 
                    // can modify this section to specify a different icon each time. 
                    // Each specified icon must be embedded as a win32 resource in the 
                    // assembly; the default one is at the index 101, but you can use 
                    // additional ones.
                    subkey.SetValue(string.Empty,
                        Assembly.GetExecutingAssembly().Location + ", 101",
                        RegistryValueKind.String);
                }


                // Create "TypeLib" to specify the typelib GUID associated with the class. 
                using (subkey = keyClsid.CreateSubKey("TypeLib"))
                {
                    var libId = Marshal.GetTypeLibGuidForAssembly(t.Assembly);
                    subkey.SetValue(string.Empty, libId.ToString("B"), RegistryValueKind.String);
                }


                // Create "Version" to specify the version of the control. 
                // Ref: http://msdn.microsoft.com/en-us/library/ms686568.aspx
                using (subkey = keyClsid.CreateSubKey("Version"))
                {
                    int major, minor;
                    Marshal.GetTypeLibVersionForAssembly(t.Assembly, out major, out minor);
                    subkey.SetValue(string.Empty, string.Format("{0}.{1}", major, minor));
                }
            }
        }

        public static void RegasmUnregisterControl(Type t)
        {
            // Check the argument
            GuardNullType(t, "t");
            GuardTypeIsControl(t);

            // Delete the CLSID key of the control
            Registry.ClassesRoot.DeleteSubKeyTree(@"CLSID\" + t.GUID.ToString("B"));
        }

        private static void GuardNullType(Type t, string param)
        {
            if (t == null)
            {
                throw new ArgumentException("The CLR type must be specified.", param);
            }
        }

        private static void GuardTypeIsControl(Type t)
        {
            if (!typeof(Control).IsAssignableFrom(t))
            {
                throw new ArgumentException(
                    "Type argument must be a Windows Forms control.");
            }
        }
    }
}