﻿namespace ActiveXPrinter
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// The ObjectSafety interface.
    /// </summary>
    [ComImport]

    // Guid corresponde a IObjectSafety. No Cambiar 
    [Guid("CB5BDC81-93C1-11CF-8F20-00805F2CD064")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IObjectSafety
    {
        [PreserveSig]
        int GetInterfaceSafetyOptions(ref Guid riid, out int pdwSupportedOptions, out int pdwEnabledOptions);

        [PreserveSig]
        int SetInterfaceSafetyOptions(ref Guid riid, int dwOptionsMask, int dwEnabledOptions);
    }
}
