﻿namespace ActiveXPrinter
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    /// <summary>
    /// The printer x.
    /// </summary>
    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]
    [Guid("3451CA6A-7517-4831-A6D5-600E54D99BC3")]
    [ProgId("ActiveXPrinter.PrinterX")]
    [ComDefaultInterface(typeof(IPrinterX))]
    public class PrinterX : UserControl, IPrinterX, IObjectSafety
    {
        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Register PrinterX

        [EditorBrowsable(EditorBrowsableState.Never)]
        [ComRegisterFunction]
        public static void Register(Type type)
        {
            try
            {
                PrinterXHelper.RegasmRegisterControl(type);
            }
            catch (Exception ex)
            {
                    Log.Error(ex.Message);
                throw;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [ComUnregisterFunction]
        public static void Unregister(Type type)
        {
            try
            {
                PrinterXHelper.RegasmUnregisterControl(type);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw;
            }
        }

        #endregion

        #region Object Safety 
        private const int INTERFACESAFE_FOR_UNTRUSTED_CALLER = 0x00000001;
        private const int INTERFACESAFE_FOR_UNTRUSTED_DATA = 0x00000002;
        private const int S_OK = 0;

        public int GetInterfaceSafetyOptions(ref Guid riid, out int pdwSupportedOptions, out int pdwEnabledOptions)
        {
            pdwSupportedOptions = INTERFACESAFE_FOR_UNTRUSTED_CALLER | INTERFACESAFE_FOR_UNTRUSTED_DATA;
            pdwEnabledOptions = INTERFACESAFE_FOR_UNTRUSTED_CALLER | INTERFACESAFE_FOR_UNTRUSTED_DATA;
            return S_OK;  // return S_OK
        }

        public int SetInterfaceSafetyOptions(ref Guid riid, int dwOptionsMask, int dwEnabledOptions)
        {
            return S_OK;  // return S_OK
        }
        #endregion
     
        #region Printer
        public string Greet()
        {
            return "Welcome to PrinterX";
        }

        public void Print()
        {
        }

        public void Msgbox(string text, string caption)
        {
            MessageBox.Show(text, caption);
        }

        public void OpenCalculator()
        {
            Process.Start("calc.exe");
        }


        #endregion
    }
}