namespace ActiveXPrinter
{
    using System.Runtime.InteropServices;

    [ComVisible(true)]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    [Guid("3E605BCB-C4D5-41E9-90EB-C3935E19A9F2")]
    public interface IPrinterX
    {
        string Greet();

        void Print();

        void Msgbox(string text, string caption);

        void OpenCalculator();
    }
}