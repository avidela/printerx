﻿namespace ActiveXDeployer.MainWindow
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Windows;

    using ActiveXDeployer.Core;
    using ActiveXDeployer.SignatureAssistant;

    using CabinetHelper;

    using log4net;

    using Microsoft.Deployment.Compression;
    using Microsoft.Win32;
    using Microsoft.WindowsAPICodePack.Dialogs;

    /// <summary>
    /// The main window view model.
    /// </summary>
    public class MainWindowViewModel : BaseViewModel
    {
        #region Fields

        /// <summary>
        /// The log.
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The firma.
        /// </summary>
        private string firma;

        /// <summary>
        /// The fuente.
        /// </summary>
        private string fuente;

        /// <summary>
        /// The destino.
        /// </summary>
        private string destino;

        /// <summary>
        /// The nombre.
        /// </summary>
        private string nombre;

        /// <summary>
        /// The password.
        /// </summary>
        private string password = "Ryaco10";
        #endregion

        #region Commands
        /// <summary>
        /// Gets or sets the buscar fuente command.
        /// </summary>
        public DelegateCommand BuscarFuenteCommand { get; set; }

        /// <summary>
        /// Gets or sets the buscar destino command.
        /// </summary>
        public DelegateCommand BuscarDestinoCommand { get; set; }

        /// <summary>
        /// Gets or sets the buscar firma command.
        /// </summary>
        public DelegateCommand BuscarFirmaCommand { get; set; }

        /// <summary>
        /// Gets or sets the generar active x command.
        /// </summary>
        public DelegateCommand GenerarActiveXCommand { get; set; }


        public DelegateCommand AsistenteDeFirmaCommand { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowViewModel"/> class.
        /// </summary> 
        #endregion

        #region Constructor

        public MainWindowViewModel()
        {
            this.BuscarFuenteCommand = new DelegateCommand(this.BuscarFuente);
            this.BuscarDestinoCommand = new DelegateCommand(this.BuscarDestino);
            this.BuscarFirmaCommand = new DelegateCommand(this.BuscarFirma);
            this.GenerarActiveXCommand = new DelegateCommand(this.GenerarActiveX);
            this.AsistenteDeFirmaCommand = new DelegateCommand(this.LanzarAsistenteFirma);
            Initialize();
        }


        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the firma.
        /// </summary>
        public string Firma
        {
            get
            {
                return this.firma;
            }

            set
            {
                this.firma = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the fuente.
        /// </summary>
        public string Fuente
        {
            get
            {
                return this.fuente;
            }

            set
            {
                this.fuente = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the destino.
        /// </summary>
        public string Destino
        {
            get
            {
                return this.destino;
            }

            set
            {
                this.destino = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Nombre.
        /// </summary>
        public string Nombre
        {
            get
            {
                return this.nombre;
            }

            set
            {
                this.nombre = value;
                this.OnPropertyChanged();
            }
        }

        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                this.password = value;
                this.OnPropertyChanged();
            }
        }
        #endregion

        #region Methods

        private void BuscarFirma(object obj)
        {
            var openFileDialog = new OpenFileDialog { InitialDirectory = this.GetFirmaInitialDirectory() };
            if (openFileDialog.ShowDialog() == true)
            {
                this.Firma = openFileDialog.FileName;
            }
        }

        private void BuscarFuente(object obj)
        {
            var dialog = this.GetOpenFolderDialog(this.GetActiveXProjectDirectory());

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                this.Fuente = dialog.FileName;
            }
        }

        private void BuscarDestino(object obj)
        {
            var dialog = this.GetOpenFolderDialog(Environment.GetFolderPath(Environment.SpecialFolder.Desktop));

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                this.Destino = dialog.FileName;
            }
        }

        private void GenerarActiveX(object obj)
        {
            bool error = false;

            if (string.IsNullOrEmpty(this.Fuente))
            {
                MessageBox.Show("La Fuente es necesaria generar el .Cab");
                error = true;
            }

            if (string.IsNullOrEmpty(this.destino))
            {
                MessageBox.Show("El destino debe ser seleccionado para generar el .Cab");
                error = true;
            }

            if (string.IsNullOrEmpty(this.Firma))
            {
                MessageBox.Show("Es necesario elegir una Firma para genera el .Cab");
                error = true;
            }

            if (string.IsNullOrEmpty(this.Nombre))
            {
                MessageBox.Show("Es necesario elegir el nombre para el archivo .Cab");
                error = true;
            }

            if (error)
            {
                return;
            }

            var nombreSinExtension = this.Nombre.Contains(".cab")
                                         ? this.Nombre.Replace(".cab", string.Empty)
                                         : this.Nombre;

            var packDestination = string.Format(@"{0}\{1}{2}", this.Destino, nombreSinExtension, ".cab");
            try
            {
                this.Pack(this.Fuente, packDestination);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                MessageBox.Show("Error al crear el .Cab");
                return;
            }

            try
            {
                Sign(packDestination, this.Firma, this.Password);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                MessageBox.Show("Error al firmar el .Cab");
                return;
            }

            var sampleWebsite = SampleWebsiteBuilder.BuildIndex(
                Guid.Parse("3451CA6A-7517-4831-A6D5-600E54D99BC3"),
                nombreSinExtension);
            var cabDestination = packDestination.Replace(".cab", string.Empty);
            var lastIndex = cabDestination.LastIndexOf(string.Format("\\{0}", nombreSinExtension), StringComparison.Ordinal);
            cabDestination = cabDestination.Remove(lastIndex);

            var sampleWebsiteDestination = string.Format("{0}\\{1}.html", cabDestination, nombreSinExtension);

            File.WriteAllText(sampleWebsiteDestination, sampleWebsite);
            MessageBox.Show(string.Format(".Cab creado con exito en el directorio : {0}", packDestination));

            Process.Start(cabDestination);
            var browser = new Process
                              {
                                  StartInfo =
                                      {
                                          FileName = "iexplore.exe",
                                          Verb = "runas",
                                          Arguments = sampleWebsiteDestination
                                      }
                              };
            browser.Start();
        }

        /// <summary>
        /// The get open folder dialog.
        /// </summary>
        /// <param name="initialDirectory">
        /// The initial Directory.
        /// </param>
        /// <returns>
        /// The <see cref="CommonOpenFileDialog"/>.
        /// </returns>
        private CommonOpenFileDialog GetOpenFolderDialog(string initialDirectory = "")
        {
            var dialog = new CommonOpenFileDialog
                             {
                                 IsFolderPicker = true,
                                 AddToMostRecentlyUsedList = false,
                                 InitialDirectory =
                                     string.IsNullOrEmpty(initialDirectory)
                                         ? Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
                                         : initialDirectory,
                                 AllowNonFileSystemItems = false,
                                 EnsureFileExists = true,
                                 EnsurePathExists = true,
                                 EnsureReadOnly = false,
                                 EnsureValidNames = true,
                                 Multiselect = false,
                                 ShowPlacesList = true
                             };
            return dialog;
        }

        /// <summary>
        /// The pack.
        /// </summary>
        /// <param name="sourceFolder">
        /// The source folder.
        /// </param>
        /// <param name="cabFilePath">
        /// The cab file path.
        /// </param>
        private void Pack(string sourceFolder, string cabFilePath)
        {
            try
            {
                var pkg = SignableCabinetPackage.LoadOrCreateCab(cabFilePath);
                pkg.Pack(
                    sourceFolder,
                    true,
                    CompressionLevel.Normal,
                    this.ProcessHandle);
                Log.Info("Packing cabinet succeeded.");
            }
            catch (Exception ex)
            {
                Log.Error("Packing cabinet failed.");
                Log.Error(ex.Message);
                throw;
            }
        }

        private void UnPack(string destinationFolder, string cabFilePath)
        {
                var pkg = SignableCabinetPackage.LoadCab(cabFilePath);
                pkg.Unpack(destinationFolder, this.ProcessHandle);
                Log.Info("Unpacking cabinet succeeded.");
        }

        private void Sign(string cabFilePath, string pfxFilePath, string password)
        {
            var pkg = SignableCabinetPackage.LoadCab(cabFilePath);
            pkg.Sign(pfxFilePath, password);
            Log.Info("Cabinet signature succeeded.");
        }

        private void ProcessHandle(object sender, ArchiveProgressEventArgs e)
        {
        }

        private void LanzarAsistenteFirma(object obj)
        {
            var signatureAssistant = new SignatureAssitantView();
            signatureAssistant.Show();
            this.IsVisible = false;
        }

        private void Initialize()
        {

        }

        private string GetFirmaInitialDirectory()
        {
            return this.GetSolutionDirectory();
        }

        private string GetSolutionDirectory()
        {
            var directory = string.Empty;
            var parentDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;
            if (parentDirectory == null)
            {
                return directory;
            }

            if (parentDirectory.Parent == null)
            {
                return directory;
            }

            if (parentDirectory.Parent.Parent != null)
            {
                directory = parentDirectory.Parent.Parent.FullName;
            }

            return directory;
        }

        private string GetActiveXProjectDirectory()
        {
            return Path.Combine(this.GetSolutionDirectory(), @"ActiveXPrinter\bin\");
        }

        #endregion
    }
}
