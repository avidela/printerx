﻿namespace ActiveXDeployer.MainWindow
{
    /// <summary>
    /// Interaction logic for MainWindowView
    /// </summary>
    public partial class MainWindowView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowView"/> class.
        /// </summary>
        public MainWindowView()
        {
            this.InitializeComponent();
            this.DataContext = new MainWindowViewModel();
        }
    }
}
