﻿namespace ActiveXDeployer
{
    using System;
    using System.Text;

    public static class SampleWebsiteBuilder
    {
        public static string BuildIndex(Guid classId, string activeXFileName)
        {
            var stringbuilder = new StringBuilder();
            stringbuilder.Append("<!DOCTYPE html>");
            stringbuilder.AppendLine();
            stringbuilder.Append("<html>");
            stringbuilder.AppendLine();
            stringbuilder.Append("<head>");
            stringbuilder.AppendLine();
            stringbuilder.AppendFormat("<title>{0}</title>", activeXFileName);
            stringbuilder.AppendLine();
            stringbuilder.Append("</head>");
            stringbuilder.AppendLine();
            stringbuilder.Append("<body>");
            stringbuilder.AppendLine();
            stringbuilder.AppendFormat("<OBJECT id=\"{0}\" classid=\"clsid:{1}\"", activeXFileName, classId);
            stringbuilder.AppendFormat("codebase=\"{0}.cab\"></OBJECT>", activeXFileName);
            stringbuilder.AppendLine();
            stringbuilder.Append("<button onclick=\"RunActiveX()\">RunActiveX</button>");
            stringbuilder.AppendLine();
            stringbuilder.Append("<script type=\"text/javascript\">");
            stringbuilder.AppendLine();
            stringbuilder.Append("function RunActiveX(){");
            stringbuilder.AppendLine();
            stringbuilder.Append("try {");
            stringbuilder.AppendLine();
            stringbuilder.AppendFormat("var obj = document.{0};", activeXFileName);
            stringbuilder.AppendLine();
            stringbuilder.Append("if (obj) {");
            stringbuilder.AppendLine();
            stringbuilder.Append("alert(obj.Greet());");
            stringbuilder.AppendLine();
            stringbuilder.Append("obj.Msgbox(\"Hola\",\"Titulo\");");
            stringbuilder.AppendLine();
            stringbuilder.Append("obj.OpenCalculator();");
            stringbuilder.AppendLine();
            stringbuilder.Append(" } else {");
            stringbuilder.AppendLine();
            stringbuilder.Append("alert(\"Object is not created!\");");
            stringbuilder.AppendLine();
            stringbuilder.Append("}");
            stringbuilder.AppendLine();
            stringbuilder.Append("} catch (ex) {");
            stringbuilder.AppendLine();
            stringbuilder.Append("alert(\"Some error happens, error message is: \" + ex.description);");
            stringbuilder.AppendLine();
            stringbuilder.Append("}}");
            stringbuilder.AppendLine();
            stringbuilder.Append("</script>");
            stringbuilder.AppendLine();
            stringbuilder.Append("</body>");
            stringbuilder.AppendLine();
            stringbuilder.Append("</html>");
            stringbuilder.AppendLine();
            return stringbuilder.ToString();
        }
    }
}