﻿namespace ActiveXDeployer.Core
{
    public class BaseViewModel : NotificationObject
    {
        private bool isVisible;
     
        public BaseViewModel()
        {
            this.IsVisible = true;
        }

        public bool IsVisible
        {
            get
            {
                return this.isVisible;
            }

            set
            {
                this.isVisible = value;
                this.OnPropertyChanged();
            }
        }

    }
}
